# Application Basket

* Notre projet est une application qui permet a des clubs de basket d'envoyer des tactiques de jeux à leurs equipes.
* Editeur et visualisateur interne des tactiques de jeux.
* Les administrateurs (coachs) d'une equipe peuvent diffuser leurs tactiques à leurs élèves.
* Les élèves peuvent aussi proposer leurs propres tactiques que les coachs peuvent à leurs tour approuver. 

## Contribuer
Dans le code ou dans la documentation, merci d'ecrire en anglais si possible
